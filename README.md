# JSONSnap

JSONSnap is a JSON Storage Saas built using Laravel, MySQL, and Vue.js. It provides a simple and efficient way to store, manage, and query JSON data for small to medium projects. With JSONSnap, you can create JSON collections, define schemas, and perform CRUD operations on the data with ease.

## Tech Stack

- Backend:
  - Laravel - a PHP framework for building robust web applications.
  - MySQL - a popular open-source relational database management system.
  - Sanctum - a Laravel package for API authentication using tokens.

- Frontend:
  - Vue.js - a progressive JavaScript framework for building user interfaces.
  - Vue Router - the official routing library for Vue.js.
  - Pinia - a state management library for Vue.js.

## Features

- User Authentication:
  - User registration and login using email and password.
  - API key generation for secure API access.

- JSON Collection Management:
  - Create, update, read, and delete JSON collections.
  - Define data types for each JSON property (String, Number, Date, Boolean, File).

- JSON Data Operations:
  - Insert, update, read, and delete JSON data within collections.
  - Validate JSON data against the collection schema before insertion.

- Role-Based Access Control:
  - Assign user roles to control access to collections.
  - Flexible permission management for each collection.

## How It Works

1. User Registration:
   - Users can create an account and receive authentication credentials.
   - The system generates a client and secret key for API access.

2. JSON Collection Creation:
   - Users can create JSON collections and define the data schema.
   - Data types (String, Number, Date, Boolean, File) can be assigned to each property.

3. JSON Data Operations:
   - Users can perform CRUD operations on the JSON data within collections.
   - API key and client key validation are enforced for data changes or reading.

4. Schema Validation:
   - The system validates the JSON schema before inserting the data.
   - Invalid data that doesn't match the schema will be rejected.

5. Role-Based Access Control:
   - Collections are protected based on user roles and permissions.
   - Users can only access and modify collections they have appropriate access to.

## Using the JavaScript file to call the API

To interact with the JSONSnap API, you can use the provided JavaScript file, `jsonsnap.js`, which contains the necessary methods for making API requests. Follow the steps below to get started:

1. Include the `jsonsnap.js` file in your project by adding the following script tag to your HTML file:

```html
<script src="path/to/jsonsnap.js"></script>
```

2. Initialize the JSONSnap object with your API key:

```javascript
const jsonSnap = new JSONSnap('YOUR_API_KEY');
```

3. Use the available methods on the `jsonSnap` object to interact with the API. For example, to read JSON data from a collection, you can call the `readData` method:

```javascript
jsonSnap.readData('collectionId', 'dataId')
  .then(response => {
    console.log('JSON Data:', response);
    // Handle the response data
  })
  .catch(error => {
    console.error('Error:', error);
    // Handle the error
  });
```

4. Adjust the method calls and parameters according to your specific requirements and API endpoints. Refer to the documentation or code comments within the `jsonsnap.js` file for more details on available methods and their usage.

5. Make sure to replace `'YOUR_API_KEY'` with your actual API key obtained from JSONSnap.

That's it! You can now use the `jsonsnap.js` file to make API calls and integrate JSONSnap functionality into your application.

## License

This project is licensed under the [MIT License](LICENSE).
