<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function validate($request, $rules) {
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }
}
