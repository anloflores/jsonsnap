<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JsonCollection;
use App\Models\User;

use App\Http\Resources\JsonCollectionResource;

class JsonCollectionController extends Controller
{
    public function create(Request $request)
    {
        $validate = $this->validate($request, [
            'name' => 'required|string',
            'schema' => 'required|json',
        ]);

        if ($validate->fails()) {
            return response()->json(['errors' => $validate->errors()], 422);
        }

        $user = $request->user();
        $jsonCollection = $user->jsonCollections()->create([
            'name' => $request->name,
            'schema' => $request->schema,
        ]);

        return new JsonCollectionResource($jsonCollection);
    }

    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'name' => 'required|string',
            'schema' => 'required|json',
        ]);

        if ($validate->fails()) {
            return response()->json(['errors' => $validate->errors()], 422);
        }


        $jsonCollection = JsonCollection::findOrFail($id);
        $jsonCollection->update([
            'name' => $request->name,
            'schema' => $request->schema,
        ]);

        return new JsonCollectionResource($jsonCollection);
    }

    public function read($id)
    {
        $jsonCollection = JsonCollection::findOrFail($id);

        return new JsonCollectionResource($jsonCollection);
    }

    public function all(Request $request)
    {
        $user = $request->user();
        $jsonCollections = $user->jsonCollections;

        return JsonCollectionResource::collection($jsonCollections);
    }

    public function delete($id)
    {
        $jsonCollection = JsonCollection::findOrFail($id);
        $jsonCollection->delete();

        return response()->json(['message' => 'JSON collection deleted successfully']);
    }
}