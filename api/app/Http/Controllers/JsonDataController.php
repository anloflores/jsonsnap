<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JsonCollection;
use App\Models\JsonData;

use App\Http\Resources\JsonDataResource;

use Validator;

class JsonDataController extends Controller
{
    public function read($collection, $id)
    {
        $jsonCollection = JsonCollection::findOrFail($collection);
        $jsonData = $jsonCollection->jsonData()->findOrFail($id);

        return new JsonDataResource($jsonData);
    }

    public function update(Request $request, $collection, $id)
    {
        $jsonCollection = JsonCollection::findOrFail($collection);
        $jsonData = $jsonCollection->jsonData()->findOrFail($id);

        $jsonData->update([
            'data' => $request->data,
        ]);

        return new JsonDataResource($jsonData);
    }

    public function delete($collection, $id)
    {
        $jsonCollection = JsonCollection::findOrFail($collection);
        $jsonData = $jsonCollection->jsonData()->findOrFail($id);
        $jsonData->delete();

        return response()->json(['message' => 'Data deleted successfully']);
    }

    public function all($collection)
    {
        $jsonCollection = JsonCollection::findOrFail($collection);
        $jsonData = $jsonCollection->jsonData;

        return JsonDataResource::collection($jsonData);
    }

    public function insert(Request $request, $collection)
    {
        $validate = $this->validate($request, [
            'data' => 'required|json',
        ]);

        if ($validate->fails()) {
            return response()->json(['errors' => $validate->errors()], 422);
        }

        $jsonCollection = JsonCollection::findOrFail($collection);
        $validator = Validator::make(json_decode($request->data, true), json_decode($jsonCollection->schema, true));

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }


        $jsonData = $jsonCollection->jsonData()->create([
            'data' => $request->data,
        ]);

        return new JsonDataResource($jsonData);
    }
}