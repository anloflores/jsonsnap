<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\JsonData;

class JsonCollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $data_count = JsonData::where('json_collection_id', $this->id)->count();

        return [
            'id' => $this->id,
            'name' => $this->name,
            'user_id' => $this->user_id,
            'schema' => json_decode($this->schema),
            'data_count' => $data_count,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
