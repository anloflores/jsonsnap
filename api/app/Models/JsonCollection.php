<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JsonCollection extends Model
{
    protected $fillable = [
        'name',
        'schema',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function jsonData()
    {
        return $this->hasMany(JsonData::class);
    }
}