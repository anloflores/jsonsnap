<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JsonData extends Model
{
    protected $fillable = [
        'data',
        'json_collection_id',
    ];


    public function jsonCollection()
    {
        return $this->belongsTo(JsonCollection::class);
    }
}