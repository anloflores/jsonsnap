<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;

    protected $fillable = [
        'name',
        'email',
        'password',
        'api_key',
    ];

    protected $hidden = [
        'password',
        'api_key',
    ];

    public function jsonCollections()
    {
        return $this->hasMany(JsonCollection::class);
    }
}