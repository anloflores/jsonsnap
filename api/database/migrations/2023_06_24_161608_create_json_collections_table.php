<?php

// CreateJsonCollectionsTable.php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJsonCollectionsTable extends Migration
{
    public function up()
    {
        Schema::create('json_collections', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->json('schema');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('json_collections');
    }
}