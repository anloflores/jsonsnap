<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('json_data', function (Blueprint $table) {
            $table->id();
            $table->json('data');
            $table->unsignedBigInteger('json_collection_id');
            $table->foreign('json_collection_id')->references('id')->on('json_collections')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('json_data');
    }
};
