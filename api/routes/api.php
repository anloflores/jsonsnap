<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\JsonCollectionController;
use App\Http\Controllers\JsonDataController;

Route::prefix('/v1')->group(function() {

    Route::post('/auth/register', [AuthController::class, 'register']);
    Route::post('/auth/login', [AuthController::class, 'login']);

    Route::middleware('auth:sanctum')->group(function () {
        Route::post('/collection/create', [JsonCollectionController::class, 'create']);
        Route::put('/collection/update/{id}', [JsonCollectionController::class, 'update']);
        Route::get('/collection/read/{id}', [JsonCollectionController::class, 'read']);
        Route::get('/collection/all', [JsonCollectionController::class, 'all']);
        Route::delete('/collection/delete/{id}', [JsonCollectionController::class, 'delete']);


        Route::put('/data/{collection}/update/{id}', [JsonDataController::class, 'update']);
        Route::delete('/data/{collection}/delete/{id}', [JsonDataController::class, 'delete']);
        Route::post('/data/{collection}/insert', [JsonDataController::class, 'insert']);
    });


    Route::middleware('api.key')->group(function () {
        Route::get('/data/{collection}/read/{id}', [JsonDataController::class, 'read']);
        Route::get('/data/{collection}/all', [JsonDataController::class, 'all']);
    });

});