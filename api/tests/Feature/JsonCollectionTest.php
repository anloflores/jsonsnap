<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class JsonCollectionTest extends TestCase
{
    use RefreshDatabase;

    protected function authenticateUser()
    {
        $user = \App\Models\User::factory()->create();

        $this->actingAs($user);
        $this->withHeaders(['Authorization' => 'Bearer ' . $user->createToken('auth_token')->plainTextToken]);
    }

    public function testCreateJsonCollection()
    {
        $this->authenticateUser();

        $response = $this->post('/collection/create', [
            'name' => 'MyCollection',
            'schema' => json_encode([
                'property1' => 'string',
                'property2' => 'number',
            ]),
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'JSON collection created successfully',
            ]);
    }

    public function testUpdateJsonCollection()
    {
        $this->authenticateUser();

        $collection = \App\Models\JsonCollection::factory()->create();

        $response = $this->put('/collection/update/' . $collection->id, [
            'name' => 'UpdatedCollection',
            'schema' => json_encode([
                'property1' => 'string',
                'property2' => 'number',
            ]),
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'JSON collection updated successfully',
            ]);
    }

    public function testReadJsonCollection()
    {
        $collection = \App\Models\JsonCollection::factory()->create();

        $response = $this->get('/collection/read/' . $collection->id);

        $response->assertStatus(200)
            ->assertJsonStructure(['json_collection']);
    }

    public function testGetAllJsonCollections()
    {
        $this->authenticateUser();

        \App\Models\JsonCollection::factory()->count(3)->create();

        $response = $this->get('/collection/all');

        $response->assertStatus(200)
            ->assertJsonStructure(['json_collections']);
    }

    public function testDeleteJsonCollection()
    {
        $this->authenticateUser();

        $collection = \App\Models\JsonCollection::factory()->create();

        $response = $this->delete('/collection/delete/' . $collection->id);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'JSON collection deleted successfully',
            ]);
    }
}