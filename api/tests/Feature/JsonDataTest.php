<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class JsonDataTest extends TestCase
{
    use RefreshDatabase;

    protected function authenticateUser()
    {
        $user = \App\Models\User::factory()->create();

        $this->actingAs($user);
        $this->withHeaders(['Authorization' => 'Bearer ' . $user->createToken('auth_token')->plainTextToken]);
    }

    public function testReadJsonData()
    {
        $collection = \App\Models\JsonCollection::factory()->create();
        $data = \App\Models\JsonData::factory()->create(['json_collection_id' => $collection->id]);

        $response = $this->get('/data/' . $collection->id . '/read/' . $data->id);

        $response->assertStatus(200)
            ->assertJsonStructure(['json_data']);
    }

    public function testUpdateJsonData()
    {
        $this->authenticateUser();

        $collection = \App\Models\JsonCollection::factory()->create();
        $data = \App\Models\JsonData::factory()->create(['json_collection_id' => $collection->id]);

        $response = $this->put('/data/' . $collection->id . '/update/' . $data->id, [
            'data' => json_encode(['property1' => 'value1']),
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'JSON data updated successfully',
            ]);
    }

    public function testDeleteJsonData()
    {
        $this->authenticateUser();

        $collection = \App\Models\JsonCollection::factory()->create();
        $data = \App\Models\JsonData::factory()->create(['json_collection_id' => $collection->id]);

        $response = $this->delete('/data/' . $collection->id . '/delete/' . $data->id);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'JSON data deleted successfully',
            ]);
    }

    public function testGetAllJsonData()
    {
        $collection = \App\Models\JsonCollection::factory()->create();
        \App\Models\JsonData::factory()->count(3)->create(['json_collection_id' => $collection->id]);

        $response = $this->get('/data/' . $collection->id . '/all');

        $response->assertStatus(200)
            ->assertJsonStructure(['json_data']);
    }

    public function testInsertJsonData()
    {
        $this->authenticateUser();

        $collection = \App\Models\JsonCollection::factory()->create();

        $response = $this->post('/data/' . $collection->id . '/insert', [
            'data' => json_encode(['property1' => 'value1']),
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'JSON data inserted successfully',
            ]);
    }
}