import { createApp } from 'vue'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

import App from './App.vue'
import router from './router'
import axios from 'axios'

import '@/assets/style.scss'

import Loader from '@/components/Loader.vue'

window.api = axios.create({
    baseURL: window.baseUrl
})

const app = createApp(App)

app.component('Loader', Loader);

const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)

app.use(pinia)
app.use(router)

app.mount('#app')
