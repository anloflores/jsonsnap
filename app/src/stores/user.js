import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useUser = defineStore('user', () => {
  const user = ref(null)

  function set(data) {
    user.value = data;
  }

  return { user, set }
}, {
  
  persist: {
    key: 'jsnap_user'
  },
})
