class JSONSnap {
    constructor(apiKey) {
        this.apiKey = apiKey;
        this.baseUrl = 'https://example.com/api';
        this.endpoints = {
            data: {
                read: `${this.baseUrl}/data/collection`,
                update: `${this.baseUrl}/data/collection`,
                delete: `${this.baseUrl}/data/collection`,
                all: `${this.baseUrl}/data/collection`,
                insert: `${this.baseUrl}/data/collection`,
            },
        };
    }

    async sendRequest(url, method = 'GET', body = null, headers = {}) {
        try {
            const response = await fetch(url, {
                method,
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${this.apiKey}`,
                    ...headers,
                },
                body: body ? JSON.stringify(body) : null,
            });

            const data = await response.json();
            if (!response.ok) {
                throw new Error(data.message || 'An error occurred.');
            }
            return data;
        } catch (error) {
            console.error(error);
            throw error;
        }
    }

    init(apiKey) {
        this.apiKey = apiKey;
    }

    async readData(collectionId, dataId) {
        try {
            const response = await this.sendRequest(`${this.endpoints.data.read}/${collectionId}/${dataId}`);
            return response;
            // Handle the JSON Data
        } catch (error) {
            // Handle errors
        }
    }

    async updateData(collectionId, dataId, newData) {
        try {
            const response = await this.sendRequest(
                `${this.endpoints.data.update}/${collectionId}/${dataId}`,
                'PUT',
                newData
            );
            return response;
            // Handle the updated JSON Data
        } catch (error) {
            // Handle errors
        }
    }

    async deleteData(collectionId, dataId) {
        try {
            const response = await this.sendRequest(`${this.endpoints.data.delete}/${collectionId}/${dataId}`, 'DELETE');
            return response;
            // Handle the deleted JSON Data
        } catch (error) {
            // Handle errors
        }
    }

    async getAllData(collectionId) {
        try {
            const response = await this.sendRequest(`${this.endpoints.data.all}/${collectionId}`);
            return response;
            // Handle the retrieved JSON Data
        } catch (error) {
            // Handle errors
        }
    }

    async insertData(collectionId, newData) {
        try {
            const response = await this.sendRequest(`${this.endpoints.data.insert}/${collectionId}`, 'POST', newData);
            return response;
            // Handle the inserted JSON Data
        } catch (error) {
            // Handle errors
        }
    }
}

// Example usage:
// const jsonSnap = new JSONSnap();

// Initialize with API key
// jsonSnap.init('YOUR_API_KEY');

// Call the methods as needed
// jsonSnap.readData('collectionId', 'dataId');
// jsonSnap.updateData('collectionId', 'dataId', { property1: 'updatedValue' });
// jsonSnap.deleteData('collectionId', 'dataId');
// jsonSnap.getAllData('collectionId');
// jsonSnap.insertData('collectionId', { property1: 'value1' });